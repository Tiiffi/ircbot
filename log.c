#include "log.h"

// NOTE (Tiiffi): What should be the action if logging function fails?
//                Should it return error value or exit immediatelly?

void log_time(char *buffer, size_t size)
{
    time_t now = time(NULL);

    if (now == -1)
    {
        internal_error("time() failed.", -4);
        exit(EXIT_FAILURE);
    }

    {
        struct tm *now_tm = localtime(&now);

        if (now_tm != NULL)
        {
            int result = strftime(buffer, size, "%d-%m-%Y %H:%M:%S", now_tm);

            if (result == 0)
            {
                internal_error("strftime() failed.", -4);
                exit(EXIT_FAILURE);
            }

            return;
        }
    }
}

FILE *log_open(const char* filename)
{
    FILE *file = fopen(filename, "ab");

    if (file == NULL)
    {
        internal_error("fopen() failed.", -4);
        exit(EXIT_FAILURE);
    }

    return file;
}

void log_close(FILE *fp)
{
    if(fp != NULL)
    {
        int result = fclose(fp);

        if (result == EOF)
        {
            internal_error("fclose() failed.", -4);
        }
    }
}

void log_write(FILE *stream, log_level_t level, const char *format, ...)
{
    va_list args;
    va_start(args, format);

    const char *err_level_str;

    switch (level)
    {
        case LOG_INFO:      err_level_str = "[INFO]  ";      break;
        case LOG_ERROR:     err_level_str = "[ERROR]  ";     break;
        case LOG_CRITICAL:  err_level_str = "[CRITICAL]  ";  break;
        case LOG_WARNING:   err_level_str = "[WARNING]  ";   break;
        case LOG_DEBUG:     err_level_str = "[DEBUG]  ";     break;
        case LOG_NOTSET:
        default:            err_level_str = "";              break;
    }

    char time_str[30];
    log_time(time_str, sizeof time_str);

    int result = fprintf(stream, "%s  %s", time_str, err_level_str);
    if (result < 0)
    {
        internal_error("fprintf() failed.", -3);
        exit(EXIT_FAILURE);
    }

    result = vfprintf(stream, format, args);
    if (result < 0)
    {
        internal_error("vfprintf() failed.", -3);
        exit(EXIT_FAILURE);
    }

    va_end(args);
}
