#ifndef NET_H_INCLUDED
#define NET_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>

#ifdef _WIN32
    //#define _WIN32_WINNT 0x0501
    #include <Winsock2.h>
    #include <ws2tcpip.h>
#else
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <netdb.h>
    #include <unistd.h>
#endif


// =============================================
//  FUNCTIONS
// =============================================

#ifdef _WIN32
void    net_init_WSA(void);
#endif

void    net_close(int sd);
int     net_connect(const char *host, const char *port);
int     net_send(int sd, const char *buffer, size_t size);


// =============================================
//  MACROS
// =============================================

#define net_send_string(SD, BUFFER) net_send(SD, BUFFER, sizeof BUFFER - 1)


#endif // NET_H_INCLUDED
