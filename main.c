#include <stdlib.h>
#include <winsock2.h>

#include "log.h"
#include "net.h"


#define LSIZE 512

// Line fetcher for quick testing
char *fetch_line(char *const buffer)
{
    static char line[LSIZE];
    static int loc = 0;
    static int i = 0;

    while(buffer[loc] != 0)
    {
        if (buffer[loc] == '\r' && buffer[loc + 1] == '\n')
        {
            line[i] = 0;
            loc += 2;
            i = 0;

            return line;
        }

        line[i++] = buffer[loc++];
    }
    loc = 0;

    return NULL;
}

int main(int argc, char *argv[])
{
    int sd = net_connect("irc.underworld.no", "6667");

    net_send_string(sd, "NICK TiiffiBot\r\n");
    net_send_string(sd, "USER TiiffiBot TiiffiBot * * :Tiiffi Bot\r\n");

    int running = 1;
    while (running)
    {
        char buffer[LSIZE] = {0};

        int result = recv(sd, buffer, sizeof buffer, 0);
        if (result == -1) break;

        while (1)
        {
            char *line = fetch_line(buffer);
            if(line == NULL) break;

            log_notset("%s\n", line);

            // Quick and dirty pong and join
            if (strncmp(line, "PING", 4) == 0)
            {
                line[1] = 'O';

                net_send(sd, line, strlen(line));
                net_send_string(sd, "\r\n");
                net_send_string(sd, "JOIN #t3st\r\n");
            }
        }
    }

    net_close(sd);

    return EXIT_SUCCESS;
}
