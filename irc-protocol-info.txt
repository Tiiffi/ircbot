https://tools.ietf.org/html/rfc2812
https://github.com/ircv3/ircv3-specifications
http://ircv3.atheme.org/
https://github.com/justintv/Twitch-API

Each IRC message may consist of up to three main parts: the prefix
(OPTIONAL), the command, and the command parameters (maximum of
fifteen (15)).  The prefix, command, and all parameters are separated
by one ASCII space character (0x20) each.


IRC Message length 512 bytes including "\r\n"
IRCV3 tags length 512 bytes including '@' and trailing spaces.


:prefix command param1 param2 :trailing string with spaces
   |    _____________________              |
   |              |                        |
optional          |                     optional
              mandatory
    command may have 0 or more params


1. parse prefix if found
2. parse command, throw error if not found
3. parse command params if found
4. parse trailing if found

White spaces longer than 1 space can be discarded in command part.


IRCv3 tags
http://ircv3.atheme.org/specification/message-tags-3.2

prototype: ['@' <tags> <SPACE>]

Example: @aaa=bbb;ccc;example.com/ddd=eee :nick!ident@host.com PRIVMSG me :Hello
