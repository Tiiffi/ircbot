#include "net.h"
#include "log.h"

#ifdef _WIN32
void net_init_WSA(void)
{
    WSADATA wsa_data;
    WORD version;

    // Requesting for WinSock version 2.2
    version = MAKEWORD(2, 2);

    int result = WSAStartup(version, &wsa_data);
    if (result != 0)
    {
        log_error("WSAStartup failed with error: %d\n", result);
        exit(EXIT_FAILURE);
    }

    // Is this needed?
    if (LOBYTE(wsa_data.wVersion) != 2 || HIBYTE(wsa_data.wVersion) != 2)
    {
        log_error("Could not find a usable version of Winsock.dll\n");
        WSACleanup();
        exit(EXIT_FAILURE);
    }

    log_info("Winsock initialized.\n");
}
#endif


// NOTE (Tiiffi): Error checking?
void net_close(int sd)
{
    #ifdef _WIN32
        closesocket(sd);
        WSACleanup();
    #else
        close(sd);
    #endif
}

int net_connect(const char *host, const char *port)
{
    int sd;

    struct addrinfo hints = {0};
    struct addrinfo *server_info, *p;

    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    #ifdef _WIN32
        net_init_WSA();
    #endif

    // Get host address info
    int result = getaddrinfo(host, port, &hints, &server_info);
    if (result != 0)
    {
        if (result == EAI_SERVICE)
        {
            log_error("Invalid port %s.\n", port);
        }
        else if (result == EAI_NONAME)
        {
            log_error("Unable to resolve hostname %s.\n", host);
        }
        else
        {
            log_error("getaddrinfo() error %d.\n", result);
        }
        exit(EXIT_FAILURE);
    }

    // Go through the hosts and try to connect
    for (p = server_info; p != NULL; p = p->ai_next)
    {
        sd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (sd == -1) continue;

        result = connect(sd, p->ai_addr, p->ai_addrlen);
        if (result == -1)
        {
            net_close(sd);
            continue;
        }

        break;
    }

    if (p == NULL)
    {
        log_error("Failed to connect.\n");
        exit(EXIT_FAILURE);
    }

    // Cheating because Windows is retarded (inet_ntop function missing)
    log_info("Connected to %s:%s.\n", host, port);

    freeaddrinfo(server_info);

    return sd;
}

int net_send(int sd, const char *buffer, size_t size)
{
    size_t sent = 0;
    size_t left = size;

    while (sent < size)
    {
        int result = send(sd, buffer + sent, left, 0);

        if (result == -1) return -1;

        sent += result;
        left -= sent;
    }

    return 0;
}
