#ifndef LOG_H_INCLUDED
#define LOG_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>


// =============================================
//  TYPES
// =============================================

typedef enum
{
    LOG_CRITICAL,
    LOG_ERROR,
    LOG_WARNING,
    LOG_INFO,
    LOG_DEBUG,
    LOG_NOTSET,
} log_level_t;


// =============================================
//  FUNCTIONS
// =============================================

void    log_time(char *buffer, size_t size);
FILE   *log_open(const char* filename);
void    log_close(FILE *fp);
void    log_write(FILE *stream, log_level_t level, const char *format, ...);


// =============================================
//  MACROS
// =============================================

// Macro fuckery to prevent the infamous trailing comma problem
#define LOG_HELPER(stream, level, format, ...)  log_write(stdout, level, format, __VA_ARGS__)

#define log_notset(...)     LOG_HELPER(stdout, LOG_NOTSET,    __VA_ARGS__, "")
#define log_info(...)       LOG_HELPER(stdout, LOG_INFO,      __VA_ARGS__, "")
#define log_error(...)      LOG_HELPER(stderr, LOG_ERROR,     __VA_ARGS__, "")
#define log_critical(...)   LOG_HELPER(stderr, LOG_CRITICAL,  __VA_ARGS__, "")

#define internal_error(x, y)    char *_INTERNAL_ERROR_FILENAME_ = strrchr(__FILE__, '\\') + 1; \
                                fprintf(stderr, "%s\nInternal error at line %d in \"%s\".\n", x, __LINE__ + (y), \
                                        _INTERNAL_ERROR_FILENAME_);

#endif // LOG_H_INCLUDED
